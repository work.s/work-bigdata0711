package cn.atuyang.work;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * @author aTuYang
 */
public class DataCountReducer extends Reducer<Text, FlowBean, Text, FlowBean>{

    FlowBean v = new FlowBean();

    @Override
    protected void reduce(Text key, Iterable<FlowBean> values, Context context) throws IOException, InterruptedException {
        int upFlow = 0;
        int downFlow = 0;
        int sumFlow = 0;

        //对上传、下载、总流量进行累加
        for (FlowBean f : values) {
            upFlow += f.getUpFlow();
            downFlow += f.getDownFlow();
            sumFlow += f.getSumFlow();
        }

        //将汇总的数据写到新的bean中，然后输出
        v.setUpFlow(upFlow);
        v.setDownFlow(downFlow);
        v.setSumFlow(sumFlow);

        context.write(key, v);
    }
}
