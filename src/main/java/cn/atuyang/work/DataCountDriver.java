package cn.atuyang.work;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

/**
 * @author aTuYang
 */
public class DataCountDriver {

    public static void main(String[] args) throws Exception {
        args = new String[]{args[0], args[1] + "/" + System.currentTimeMillis()};

        Configuration conf = new Configuration();

        //获取job对象
        Job job = Job.getInstance(conf, "yangxunyao");

        //配置driver，map，reduce类
        job.setJarByClass(DataCountDriver.class);
        job.setMapperClass(DataCountMapper.class);
        job.setReducerClass(DataCountReducer.class);

        //指定map和reduce的输出类
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(FlowBean.class);
        job.setOutputKeyClass(FlowBean.class);
        job.setOutputValueClass(Text.class);

        //指定输入数据，输出路径
        FileInputFormat.setInputPaths(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        //提交job
        job.waitForCompletion(true);
    }
}
