package cn.atuyang.work;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * @author aTuYang
 */
public class DataCountMapper extends Mapper<LongWritable, Text, Text, FlowBean>{
    Text k = new Text();
    FlowBean v = new FlowBean();

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String line = value.toString();
        String[] fields = line.split("\t");

        //开始解析切割数据
        k.set(fields[1]);
        int downFlow = Integer.parseInt(fields[fields.length - 2]);
        int upFlow = Integer.parseInt(fields[fields.length - 3]);
        v.setDownFlow(downFlow);
        v.setUpFlow(upFlow);
        v.setSumFlow(upFlow + downFlow);

        context.write(k, v);
    }
}
